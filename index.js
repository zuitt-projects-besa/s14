/*

	Lesson Proper

	What is JavaScript?

		Javascript is a scripting programming language that enables us to make interactive web pages.

	Comments

		in JS there are to ways to add comments:

		// or ctrl + / - creates a single line comment.


*/
/*
		ctrl + shift + / - creates a multi line comment.

		Multi Line comments allows us to add multiple lines in a singe comma.
*/




// singe line comment - allows only for a single line of comment

console.log ("Hello, World!");

/*
	console.log() allows us to show/display data in our console. The console is a part of our browser with which we can use to display data.
*/


console.log ("Vincent Besa");
console.log ("Pizza");


/*

	Statement and Syntax

	Statement 
	- are instruction/expression we add to our script/program which will then be communicated to our computers.
	Our computers will then be able to interpret these instructions and perform the task accordingly.

	Example:
	console.log("Vincent Besa");
		We are actually telling the computer using the JS to:
			"Display/Log this data in the console."

	Most programming languages end their statement in semicolon (;). However, JS does not requir a semi-colon.

	Syntax
	- in programming, is a set of rules that describes how statements or instructions are properly made/constructed.

	For your program/syntax to work, we should be able to follow a certain set of rules.
	
	Correct Syntax:
		console.log(<data>);

	Wrong Syntax:
		(data)console.log
			"This data console display"

*/


/*

	Variables

	In HTML, elements are containers for text and other elements.

	In Javascript, variables are containers of data.

	This will then allow us to save data within our script or program.

	To create a variable, we use let keyword and the assignment operator (=).
	
	let variableName= "data";

*/

let name = "Vincent Besa";

// log the value of variable name in the console:
console.log(name);

// save numbers in variables:
let num = 5;
let num2 = 10;

// log the values of the variable in the console:
console.log(num);
console.log(num2);

// When console.logging variables, the name of the variable will not be show, what will be shown/displayed is are the data within the variables.

// You could also check/display the values of multiple variables:
// console.log(variable1,variable2);

console.log(name,num,num2);


// We cannot display the value of a variable that is not yet created/declared.
// In fact, that will result in an error. (not defined)
// console.log(name2);

let myVariable;

/*
	You can actually create variables without providing an initial value. However, that variable will be assigned as "undefined".
	Because we don't know the value yet, because we haven't provided a value yet. You can actually add the value later.
*/

console.log(myVariable); //result: undefined.

/*

	Creating variable is actually 2 steps:

	1. Declaration - It is the declaration/creation of the variable with either the let and const keyword.
	2. Initialization - Is when we provide an initial value to our variable.
	
	Declaration = Initialization
	let myVar = "Initial Value";

*/

myVariable = "New Value";
console.log(myVariable);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

// You can update the value of a variable declared using the let keyword
bestFinalFantasy = "Final Fantasy 7"
console.log(bestFinalFantasy);

/*let bestFinalFantasy = "Final Fantasy 6"
console.log(bestFinalFantasy);
*/

/*

	Const

	const keyword allows us to create a variable like let, however, the const variable cannot be updated.
	Values in a const variable cannot be changed. You also cannot create a const variable without initialization.

*/



const pi = 3.1416;
console.log(pi);

// You cannot set a new value to an const variable
/*pi = 3.15;
console.log(pi);*/



// Declare const variable without initialization results to an error
/*const plateNum;
console.log(plateNum);
*/



let name2 = "Edward Cullen";
let role = "Supervisor";

role = "Director";

const tin = "1233-1234";


console.log(name2,role,tin);
console.log(role);


/*

	Convention in creating variable/constant names:

	To create a let variable, we use the let keyword, to create a const variable we use const keyword.

	variables declared with let, its values can be updated. Variables declared with const, we cannot change the value.

	let variables and const variable are usually named in small caps. Because there are other JS keyword that start in a capital letters.

	If you want to name your variable with multiple words, we can use camelCase.
	camel.Case is a convention in writing variable by adding the first word in small caps and the following words starting with capital letters.

	variable names define the values they contain. Name your variables appropriately.

*/

/*

	Data Types

	In most programming language, data is differentiated into different types and we can do different things about this data. For most programming languages
	, we have to declare the data type of our data before we are able to create the variable and store.
	
	However, JS is not strict when it comes to data types.

	There are data types wherein we need to use literals to create them.
	To create strings, we use string literals like '' or ""
	To create objects, we can use object literals like {}
	To createa arrays, we can use array literals like []

*/



/*

	Strings
		- are a series of alphanumeric characters that create a word, a phrase, a name or anything that is related to creating a text.

		Strings are NOT and should not be used for mathematical operations.

		Strings are created with string literals like single quotes ('') or double quotes ("")

*/

let country = "Philippines";
let province = "Metro Manila";

console.log(province, country);

/*

	You can actually combine strings into a single string, with the use of the plus sign (+) or addition operator.

	This process is called concatonation.

*/

// In strings, spaces and commas count as characters
let address = province + " , " + country;
console.log(address);

let city1 = "Manila";
let city2 = "Capenhagen";
let city3 = "Washington D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A.";
let country3 = "South Korea";
let country4 = "Japan";


let capital1 = city1 + " is the capital of " + country1;
let capital2 = city3 + " is the capital of " + country2;
let capital3 = city4 + " is the capital of " + country3;

console.log(capital1);
console.log(capital2);
console.log(capital3);


/*

	Number/Integers

	Integers are number data which can be used for mathematical operations.
	To create a number data, add a numeric character but with no "" or ''.

*/



let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

/*

	Addition Operator

	(+) when used between 2 number types, it will add both numbers. The result of the addition can also be saved in a variable.

*/

let sum1 = number1 + number2;

console.log(sum1);

let sum2 = 16 + 4;
console.log(sum2);


let sumString = "30";

let numString2 = "50";
/*Thses are numeric strings, they are composed numeric characters but are considered a string, because it is surrounded/created with double quotes ("")*/

let sumString1 = sumString + numString2;
console.log(sumString1);
/*When numeric strings are used with +, it will not add as numbers but add as strings.*/

let sum3 = number1 + numString2;
console.log(sum3);



/*

	Boolen (true or false)

	- is usually used for logical operations and if-else conditions.
	When creating a variable that hold a boolen, the variable is usually a yes or no question.

*/


let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Curry an MVP? " + isMVP);
console.log("Is he the current admin? " + isAdmin);


/*

	Arrays

	- is a special kind of data type. It is used to store multiple values.
	Arrays can actually store values of different data type, however, this is not a good practice
		because there are array methods or ways to manipulate array which might be affected or cause conflict.
	Arrays are usually used to store multiple values of the same data type.
	There are only used cases wherein arrays are used with multiple values of different types.

	Arrays are created using an array literal or square brackets []
	Values in array are seperated by a comma.

*/

// This is a good array which:
	// The values have the same data type
	// The values share the same theme
let array1 = ["Goku", "Gohan", "Goten", "Vegeta", "Trunks", "Broly"];
let array2 = ["One Punch Man", "Saitama", true, 1000];


console.log(array1);
console.log(array2);
console.log(array1[1]);


/* In the next session, we will learn about methods which will allow us to manipulate an array. Having an array with different data types may
	obstruct to these method.*/

// Array are bext thought of as a group of data






/*

	Objects

	- are another special kind of data type used to mimic real world items/objects.

	Is is used to create complex data structure that contain pieces of information that relate to each other.
	
	Each field in an object is called property. Each properties are seperated by comma.
	
	Each property is a pair of key: value

	Object can actually group different data types.

	Each data is given more significance because each data/value has ket which defines/describes the data.

*/


/*let hero1 = {

	heroName: "One Punch Man",
	realName: "Saitama",
	income: 5000,
	isActive: true

}*/



let bandBeatles = ["John Lennon","Paul McCartney","George Harrison","Ringo Starr"]


let johnLennon = {

	firstName: "John",
	lastName: "Lennon",
	isDeveloper: false,
	age: 40,
}

console.log(bandBeatles);
console.log(johnLennon);




/*

	Null and Undefined

	-is the explicit absence of data/value. This is done to show the variable actually contains nothing as opposed to undefined
		which means that the variable has been declared or create but there was no initial value.

		Null explicit absence value.
		Undefined there is no value YET.

	Uses Cases of Null:

		When doing a query or search, there, of course might a 0 result.
			ex: let foundResult = null;

	Undefined

	- is a representation that a variable has been created or declared but there is no initial value, so, we can't quite say what the value,
		thus it is undefined.
			ex: sampleVariable; - declaration with no initial value results to undefined.

		For undefined, this is normally caused by developers when creating variables, that have no data or value associated or initialized with them

*/

let person2 = {
	name: "Peter",
	age: 42
}

// Access or display the value of an object's property we use dot notation:
// objectName.propertyName

console.log(person2.name);
console.log(person2.age);

// Undefined, beause the person2 variable does exist, however, there is no property in the object called isAdmin. 
console.log(person2.isAdmin);


/*

	Functions

	- in JS, are lines/blocks of code that tell our device/application to perform a certain task when called or invoked.

	Functions are reusable pieces of code with instructions/statements which can be used over and over again just so long as we call or use it.

	You can think of functions as programs within program.

*/

console.log("Good Afternoon, Everyon! Welcome to my application!");

// functions are created by declaring the function using the function keyword.

function greet(){
	console.log("Good Afternoon, Everyon! Welcome to my application!");
}


// function invocation - function invocation is when we call or use our funcations.
greet();
greet();





let favoriteFood = "Pizza";
let sum4 = 150 + 9;
let product = 100 * 90;
let isActive = true;

let restaurant = ["Xin Tian Di", "Seven Corners", "Cafe Inggo 1587", "Misto", "Vikings"];


let favoriteSinger = {
	firstName: "Tyler",
	lastName: "Joseph",
	stageName: "Tyler",
	birthDay: "December 1, 1988",
	age: "33",
	bestAlbum: "Vessel",
	bestSong: "House of Gold",
	bestTvShow: null,
	bestMovie: null,
	isActive: true
}

console.log(favoriteFood);
console.log(sum4);
console.log(product);
console.log(isActive);
console.log(restaurant);
console.log(favoriteSinger);


// Continuation of Functions:

	// Parameters and Arguments

	// "name" is called parameter
	// Parameter acts as a named variable/container that exists only in the function. This is used to store information to act as
	// a stand-in or the container of the value passed into the function as an argument.

	function printName(name){
		console.log(`My Name is ${name}`);
	};

// Data passed in the function: argument
// Representation of the argument within the function: parameter

printName("Jake");

function displayNum(number){
	console.log(number);
};

displayNum(3000);
displayNum(3001);


function message(message) {
	console.log(message);
};

message("Javascript is fun!");

// Multiple Parameters Arguments
	// Function cannot only receive a single argument but it can also receive multiple arguments as long as it matches the number of parameters in the function

	function displayFullName(firstName, lastName, age) {
		console.log(`${firstName} ${lastName} is ${age}`);
	};

displayFullName("Vincent", "Besa", 23);


// return keyword
	// return keyword is used so that a function may return a value
	// it also stops the process of the function after the return keyword

	function createFullName (firstName, middleName, lastName,){
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value result has been returned.")
	};

	let fullName1 = createFullName("Tom", "Cruise", "Mopother");
	console.log(fullName1);